
import React from 'react';

export const Restore = props => (
  <svg viewBox="0 0 20 20" {...props}><path d="M-0.46-0.6v513.46h513.46V-0.6H-0.46z M444.01,444.01H68.17V68.17h375.84V444.01z" fillRule="evenodd" /></svg>
);
