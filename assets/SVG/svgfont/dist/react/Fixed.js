
import React from 'react';

export const Fixed = props => (
  <svg viewBox="0 0 20 20" {...props}><path d="M-.46-.6V512.86H513V-.6ZM444,444H68.17V68.17H444Z" fillRule="evenodd" /></svg>
);
