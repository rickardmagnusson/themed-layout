const spinner = document.body.querySelector('.spin-wave');


window.addEventListener('DOMContentLoaded', event => {


    // Toggle the side navigation
    const bt = document.body.querySelector('.maximize');
    const props = document.getElementById('properties');

    if (bt) {
        bt.addEventListener('click', event => {

            if(props.classList.contains('full')){
                bt.classList.remove('bt-font-restore');
                bt.classList.add('bt-font-max');
                props.classList.remove('full');
            }else{
                bt.classList.add('bt-font-restore');
                bt.classList.remove('bt-font-max');
                props.classList.add('full');
            }
           event.preventDefault();
        });
    }


    var tbodyRef = document.querySelector("tbody");
      
    for(var i=0; i< 200; i++){
    
        var newRow = tbodyRef.insertRow();

        var newCell = newRow.insertCell();
        var newText = document.createTextNode('Cell ' + i);
        newCell.classList.add('indexcell');
        newCell.appendChild(newText);

         newCell = newRow.insertCell();
        var newText = document.createTextNode('Cell ' + (i+1));
        newCell.classList.add('fat');
        newCell.appendChild(newText);

        newCell = newRow.insertCell();
        var newText = document.createTextNode('Cell ' + (i+2));
        newCell.appendChild(newText);

        newCell = newRow.insertCell();
        var newText = document.createTextNode('Cell ' + (i+4));
        newCell.appendChild(newText);
    }

    var rows = tbodyRef.querySelectorAll('tr');

   
   
    for (var i = 0; i < rows.length; i++)
    {
        rows[i].addEventListener('pointerdown', function(evt){
            
            spinner.classList.remove('hidden');
            clear();
            var td = evt.target;
            var row = td.parentElement;
            row.classList.toggle('active-row');
            document.getElementById('info-panel').classList.remove('hidden');
            console.log(td)
            document.getElementById('title').textContent= 'Viewing content of cell ' + td.textContent;
            setTimeout(function(){ spinner.classList.add('hidden'); }, 100);
        });
    }   

    function clear(){
        document.getElementById('info-panel').classList.add('hidden');
        var class_name='active-row';

        elements=document.getElementsByClassName(class_name)

        for(element of elements){
        element.classList.remove(class_name)
        }
    }
    setTimeout(function(){ spinner.classList.add('hidden'); }, 500);

    

});
